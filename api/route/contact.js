const express = require('express')
const route = express.Router();
const contactController = require('../controllers/contact');

route.get('/', contactController.getAllContactLists);
route.post('/', contactController.storeContactList);
route.get('/:id', contactController.getSingleContact);
route.put('/:id', contactController.updateContact);
route.delete('/:id', contactController.deleteContact);

module.exports = route