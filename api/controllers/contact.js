const Contact = require('../model/Contact');


const getAllContactLists = (req, res, next) => {
    Contact.find()
        .then( contacts => {
            res.status(200).json({
                message: "All Contacts List",
                contacts: contacts
            })
        })
        .catch( err => {
            console.log(err);
            res.status(500).json({
                message: "Error Occured",
                Error: err
            })
        })
}

const storeContactList = (req, res, next) => {

    const contact = new Contact({
        name: req.body.name,
        email: req.body.email,
        phone: req.body.phone
    });
    
    contact.save()
        .then( contact => {
            res.status(201).json({
                message: "Data Inserted Successfully",
                contact: contact,
            })
        })
        .catch( err => {
            console.log(err),
            res.status(500).json({
                message: "Error Ocured",
                Erorr: err
            })
        })

}

const getSingleContact = (req, res, next) => {
    Contact.findById(req.params.id)
        .then( contact => {
            res.status(200).json({
                message: "Single Information",
                contact: contact
            })
        })
        .catch( err => {
            console.log(err);
            res.status(500).json({
                message: "Error Occured",
                Error: err
            })
        })
}


const updateContact = (req, res, next) => {
    let editedData = {
        name: req.body.name,
        email: req.body.email,
        phone: req.body.phone
    }

    Contact.findByIdAndUpdate(req.params.id, { $set: editedData})
        .then( contact => {
            Contact.findById(contact._id)
                .then( result => {
                    res.json({
                        message: "Data Updated Succussfully",
                        contact
                    })
                })
        })
        .catch( err => {
            console.log(err)
            res.json({
                message: "Error Occured",
                Error: err
            })
        })
}

const deleteContact = (req, res, next) => {
    Contact.findByIdAndRemove(req.params.id)
        .then( contact => {
            res.josn({
                message: "Contact Deleted Successfully",
                contact
            })
        })
        .catch( err => {
            console.log(err);
            res.json({
                message: "Error Occured",
                Error: err
            })
        })

}


module.exports = {
    getAllContactLists,
    storeContactList,
    getSingleContact,
    updateContact,
    deleteContact
}