const mongoose = require('mongoose');
const validator = require('validator');
const Schema = mongoose.Schema;

const ContactSchema = new Schema({
    name: {
        type: String,
        trim: true,
        required: true,
        minlength: 4,
    },
    email: {
        type: String,
        trim: true,
        unique: true,
        required: true,
        validate: {
            validator: (v) => {
                return validator.isEmail(v)
            },
            message: `(VALUE) is not valid email`
        }        
    },
    phone: {
        type: String,
        trim: true,
        required: true,
        unique: true,
        validate: {
            validator: (v) => {
                return validator.isMobilePhone(v)
            },
            message: `(VALUE) is not valid mobile number`
        }
    }
})


const Contact = mongoose.model('Contact', ContactSchema);
module.exports = Contact;
