const express = require('express');
const app = express();
const port = process.env.port || 3000;
app.listen(port, () => 
    console.log(`Example app listening on port ${port}!`)
);

const mongoose = require('mongoose');
mongoose.connect('mongodb://localhost/test');
const db = mongoose.connection;

db.on('error', (err) => {
    console.log(err);
})

db.once('open', () => {
    console.log('Connection Established');    
})



const morgan = require('morgan');
app.use(morgan('dev'));

const bodyparser = require('body-parser');
app.use(bodyparser.urlencoded({extended: true}))
app.use(bodyparser.json());

const contactRoute = require('./api/route/contact');
app.use('/api/contacts', contactRoute);
