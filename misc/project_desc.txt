1) Identify Object Model
   USER
	-name
	-id
	-email
	-password
	-birthday
	-phoneno

   POST
	-title
	-contentBody
	-[comments]
	-likes
	-created_at
	-updated_at


2) Create Model Uri
   https://example.com/api/users GET
   https://example.com/api/users/:id POST

   https://example.com/api/user POST
   https://example.com/api/users/:id PUT/PACTCH
   https://example.com/api/users/:id DELETE


   https://example.com/api/posts GET
   https://example.com/api/posts/:id POST

   https://example.com/api/post POST
   https://example.com/api/posts/:id PUT/PATCH
   https://example.com/api/posts/:id DELETE




3) determine representation
[
    {
        id: 1
        name: 'Mohammad Sumon',
        email: 'sumonsbgc@gamil.com',
        password: '974410',
        birthday: '12/12/2019'
        phone: '01516-120343'
    },
    {
        id: 1
        name: 'Mohammad Sumon',
        email: 'sumonsbgc@gamil.com',
        password: '974410',
        birthday: '12/12/2019'
        phone: '01516-120343'
    },
    {
        id: 1
        name: 'Mohammad Sumon',
        email: 'sumonsbgc@gamil.com',
        password: '974410',
        birthday: '12/12/2019'
        phone: '01516-120343'
    }
]
4) single data representation
{
    id: 1
    name: 'Mohammad Sumon',
    email: 'sumonsbgc@gamil.com',
    password: '974410',
    birthday: '12/12/2019'
    phone: '01516-120343'
}


5) Json View